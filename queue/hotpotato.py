from pythonds import Queue

def hotPotato(namelist, num):
    simqueue = Queue()
    for name in namelist:
        simqueue.add(name)
    
    while simqueue.size() > 1:
        for i in range(num):
            simqueue.add(simqueue.remove())

        simqueue.remove()    
    
    return simqueue.remove()

print(hotPotato(["Bill","David","Susan","Jane","Kent","Brad"],7))

