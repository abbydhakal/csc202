"""
  Create a max heap and use it for heap sort
"""


def heapify(array, pos):
    left = 2 * pos + 1
    right = 2 * pos + 2
    last = len(array) - 1

    largest_pos = left if left <= last and array[left] > array[pos] else pos
    if right <= last and array[right] > array[largest_pos]:
        largest_pos = right
    if largest_pos != pos:
        array[pos], array[largest_pos] = array[largest_pos], array[pos]
        heapify(array, largest_pos)


def build_heap(array):
    last_parent_pos = (len(array) - 2) // 2
    for pos in range(last_parent_pos, -1, -1):
        heapify(array, pos)


def add_to_heap(heap, item):
    heap.append(item)
    parent_pos = len(heap) - 2
    while parent_pos > 0:
        parent_pos //= 2
        heapify(heap, parent_pos)


def remove_max(heap):
    heap[0], heap[-1] = heap[-1], heap[0]
    old_max = heap[-1]
    del heap[-1]
    heapify(heap, 0)
    return old_max
   

if __name__ == "__main__":
    import doctest
    doctest.testmod()

heap = [99, 42, 12, 39, 7, 9, 11, 2, 37]
a = remove_max(heap)
print(a)
