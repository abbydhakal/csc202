from statemachine import StateMachine

# create state machine called m
def not_in_ould(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no match"
    if current_input == "\n":
        state = "o"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def o_state(text):
    current_input == "\n"
    if current_input == "\n":
        state = "no_match"
    if current_input == "u":
        state = "u"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def u_state(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "l":
        state = "l"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def l_state(text):
    current_input == text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "d":
        state = "yes_match"
    else:
        state = "not_in_ould"
    return (state, text[1:])

m = StateMachine()

# add states
m.add_state("o", o_state)
m.add_state("u", u_state)
m.add_state("l", l_state)
m.add_state("not_in_ould", not_in_ould)
