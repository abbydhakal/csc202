#include <stdio.h>

void increment(int*);

int main()
{
    int x = 0;
    increment(&x);
    printf("Value in main(): %d \n", x);
}

void increment(int* a )
{
    (*a)++;
    printf("Value in increment(): %d \n", *a);
}
