from sort_tools import *
from random import sample
from time import monotonic as mono

tup_10 = tuple(sample(list(range(10)), 10))
tup_100 = tuple(sample(list(range(100)), 100))
tup_1k = tuple(sample(list(range(1000)), 1000))
tup_10k = tuple(sample(list(range(10000)), 10000))

def get_time(func, tup):
    lis = list(tup)
    start = mono()
    func(lis)
    return mono() - start

times = []

for f in [bubble_sort, selection_sort, insertion_sort,
        shell_sort, merge_sort, quick_sort]:
    for t in [tup_10, tup_100, tup_1k, tup_10k]:
        times.append(get_time(f, t))

print(f"Algorithm\tLength\t{'Time (ms)':>10}\n")

for i, name in enumerate(["Bubble Sort", "Selection Sort", "Insertion Sort", "Shell Sort", "Merge Sort", "Quick Sort"]):
    for x in range(4):
        current_time = times[(4 * i) + x]
        low_time = times[4 * i]
        print(f"{name}\t10{'0'*x}\t{f'{current_time*1000:.2f}':>10}", end="\t")
        if x % 4 != 0:
            prev_time = times[(4 * i) + x - 1]
            print(f"{f'{round(current_time/prev_time)}x last time':>15}", end="\t")
            print(f"{f'{round(current_time/low_time)}x len 10':>17}")
        else:
            print()
    print()

