# program that runs the algorithms

from sort_tools import *
from random import sample
from time import monotonic as mono

tup_10 = tuple(sample(list(range(10)), 10))
tup_100 = tuple(sample(list(range(100)), 100))
tup_1k = tuple(sample(list(range(1000)), 1000))

def get_time(func, tup):
    lis = list(tup)
    start = mono()
    func(lis)
    return mono() - start

times = []

for f in [bubble_sort, selection_sort, insertion_sort,
        shell_sort, merge_sort, quick_sort]:
    for t in [tup_10, tup_100, tup_1k]:
        times.append(get_time(f, t))

print("Algorithm\tLength\tTime (ms)\n")

for i, name in enumerate(["Bubble Sort", "Selection Sort", "Insertion Sort", "Shell Sort", "Merge Sort", "Quick Sort"]):
    for x in range(3):
        print(f"{name}\t10{'0'*x}\t{times[(3*i)+x]*1000:.2f}", end="\t")
        if x % 3 != 0:
            print(f"{round(times[(3*i)+x]/times[(3*i)+x-1])} times longer")
        else:
            print()
    print()

