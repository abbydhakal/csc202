# code for how the sorting is done
def bubble_sort(a_list):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> bubble_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    exchanges = True
    pass_num = len(a_list) - 1

    while pass_num > 0 and exchanges:
        exchanges = False
        for i in range(pass_num):
            if a_list[i] > a_list[i + 1]:
                exchanges = True
                a_list[i], a_list[i + 1] = a_list[i + 1], a_list[i]


def selection_sort(a_list):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> selection_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    for fslot in range(len(a_list) - 1, 0, -1):
        max_pos = 0

        for loc in range(1, fslot + 1):
            if a_list[loc] > a_list[max_pos]:
                max_pos = loc

        a_list[fslot], a_list[max_pos] = a_list[max_pos], a_list[fslot]


def insertion_sort(a_list, start=0, gap=1):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> insertion_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    for i in range(start + gap, len(a_list), gap):
        val = a_list[i]
        pos = i

        while pos >= gap and a_list[pos - gap] > val:
            a_list[pos] = a_list[pos - gap]
            pos -= gap

        a_list[pos] = val


def shell_sort(a_list):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> shell_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    sublist_count = len(a_list) // 2

    while sublist_count > 0:
        for start in range(sublist_count):
            insertion_sort(a_list, start, sublist_count)

        sublist_count //= 2


def merge_sort(a_list):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> merge_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    if len(a_list) > 1:
        mid = len(a_list) // 2
        left = a_list[:mid]
        right = a_list[mid:]

        merge_sort(left)
        merge_sort(right)

        i = j = k = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                a_list[k] = left[i]
                i += 1
            else:
                a_list[k] = right[j]
                j += 1
            k += 1

        while i < len(left):
            a_list[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            a_list[k] = right[j]
            j += 1
            k += 1


def partition(a_list, first, last):
    """
      >>> part_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> partition(part_list, 0, 8)
      5
      >>> part_list
      [31, 26, 20, 17, 44, 54, 77, 55, 93]
    """
    pivot = a_list[first]

    left = first + 1
    right = last
    done = False

    while not done:
        while left <= right and a_list[left] <= pivot:
            left += 1
        while a_list[right] >= pivot and right >= left:
            right -= 1
        if right < left:
            done = True
        else:
            a_list[left], a_list[right] = a_list[right], a_list[left]

    a_list[first], a_list[right] = a_list[right], a_list[first]

    return right


def quick_sort(a_list, first=None, last=None):
    """
      >>> the_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
      >>> quick_sort(the_list)
      >>> the_list
      [17, 20, 26, 31, 44, 54, 55, 77, 93]
    """
    if first is None:
        quick_sort(a_list, 0, len(a_list) - 1)
    elif first < last:
        split = partition(a_list, first, last)
        quick_sort(a_list, first, split - 1)
        quick_sort(a_list, split + 1, last)


if __name__ == '__main__':
    import doctest
    doctest.testmod()


