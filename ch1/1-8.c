#include <stdio.h>
  
/* count lines in input */
int main()
{
    int blankCount = 0, tabCount = 0, newLineCount = 0;

    char c;

    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++newLineCount;
	if (c == '\t')
	    ++tabCount;
	if (c == ' ')
	    ++blankCount;
    printf("%d %d %d\n", blankCount, tabCount, newLineCount);
}
