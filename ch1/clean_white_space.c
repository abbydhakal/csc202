#include <stdio.h>

#define INSPACE 1    /* inside a word */
#define OUTSPACE 0   /* outside a word */

int main()
{
    int state = OUTSPACE;
    int c;

    while ((c = getchar()) != EOF){
        if (state == OUTSPACE)
            if (c == ' ' || c == '\t' || c == '\n')
            {
                putchar(' ');
                state = INSPACE;
            }
            else
                putchar(c);
        else /* state == INSPACE */
            if (c != ' ' && c != '\t' && c != '\n')
        {
            putchar(c);
            state = OUTSPACE;
        }
    }
    return 0;
}
