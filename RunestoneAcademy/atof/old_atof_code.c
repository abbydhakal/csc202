#include <stdio.h>
#include "mystdio.h"
#include <ctype.h>

#define MAXLINE 100

/* atof: convert string s to double */
double atof(char s[])
/* function skips white spaces, records the sign, and calculates the number*/
{
    double val, power;
    int i, sign, exp;

    for (i = 0; isspace(s[i]); i++)   /* skip white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
        i++;
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i ++;
    for (power = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        power *= 10.0;
    }
    return sign * val / power;
}

/* rudimentary adding machine */
/* handles the optional exponent */
/* if exponent exists, then the sign of the exponent is stored in the var sign and the value of the exponent is calculated and stored in exp */

int main()
{
    double sum, atof(char []);
    char line[MAXLINE];

    sum = 0;
    while (mygetline(line, MAXLINE) > 0)
        printf("\t%g\n", sum += atof(line));

    char input[MAXLINE];
    int length = mygetline(input, MAXLINE);
    char base[20];
    char exponent[20];
    int j;
    for (j = 0 ; j < length && s[j] != ‘e’; j++)
        base[j] = input[j];
    j++;
    for (int k = j < length && j != ‘\n’ && j != EOF; j++)
        exponent[j-k] = input[j];


    return 0;
}
