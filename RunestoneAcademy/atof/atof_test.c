#include <stdio.h>
#include "mystdio.h"
#include <ctype.h>

#define MAXLINE 100

/* atof: convert string s to double */
int main()
{
    double atof(char s[])
/* function skips white spaces, records the sign, and calculates the number*/
    double val, power;
    int i, sign, exp;
    
    for (i = 0; isspace(s[i]); i++)   /* skip white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
        i++; 
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i ++;
    for (power = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        power *= 10.0;
    }    
    val = sign * val / power;

/* handles the optional exponent */
/* if exponent exists, then the sign of the exponent is stored in the var sign and the value of the exponent is calculated and stored in exp */

    if (s[i] == 'e' || s[i] == 'E'){
        sign = (s[++i] == '-') ? -1 : 1;
        if (s[i] == '+' || s[i] == '-')
            i++;
        for (exp = 0; isdigit(s[i]); i++)
            exp = 10 * exp + (s[i] - '0');
        if (sign == 1)
            while (exp -- > 0) /* positive exponent */
                val *= 10;
        else
            while (exp -- > 0) /* negative exponent */
                val /= 10;
    }
    return val;
}
