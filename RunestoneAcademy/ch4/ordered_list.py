class OrderedList:
    def __init__(self):
        self.myList = []

    def isEmpty(self):
        return self.myList == []

    def size(self):
        return len(self.myList)

    def search(self, item):
        return item in self.myList

    def add(self, item):
        if self.isEmpty():
            self.myList = [item]
            return
        if self.myList[0] > item:
            self.myList = [item] + self.myList
            return
        if self.myList[-1] < item:
            self.myList = self.myList + [item]
            return
        for i in range(1, len(self.myList)):
            if self.myList[i] > item and self.myList[i-1] < item:
                self.myList.insert(i, item)
                return

    def remove(self, item):
        if item in self.myList:
            self.myList.remove(item)

    def index(self, item):
        return self.myList.index(item)

    def pop(self, index=None):
        if index == None:
            return self.myList.pop()
        else:
            num = self.myList[index]
            self.myList = self.myList[:index] + self.myList[index+1:]
            return num

    def __str__(self):
        return str(self.myList)

ord = OrderedList()
print(ord)
ord.add(3)
ord.add(9)
ord.add(7)
print(ord)
print(ord.pop())
print(ord)
ord.add(99)
ord.add(37)
print(ord)

