#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int data;
    struct Node* next;
};
void push(struct Node** head_ref, int new_data)
{
    struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));

    new_node->data = new_data;
    new_node->next = (*head_ref);

    (*head_ref) = new_node;
}

int pop(struct Node** head_ref)
{
    int data = (*head_ref)->data;
    struct Node* new_node = (*head_ref)->next;

    free(*head_ref);
    *head_ref = new_node;

    return data;
}

void insert(struct Node** head_ref, int index, int data)
{
    struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));

    new_node->data = data;

    if (index == 0)
    {
        new_node->next = *head_ref;
        *head_ref = new_node;
    }
    else
    {
        struct Node* before = *head_ref;
        for (int i = 0 ; i < index - 1 ; i ++)
            before = before->next;

        new_node->next = before->next;
        before->next = new_node;
    }
}

void delete(struct Node** head_ref, int index)
{
    if (index == 0)
    {
        struct Node* new_head_ref = (*head_ref)->next;
        free(*head_ref);
        *head_ref = new_head_ref;
    }
    else
    {
        struct Node* before = *head_ref;
        for (int i = 0 ; i < index - 1 ; i++)
            before = before->next;
        struct Node* to_be_removed = before->next;
        before->next = (before->next)->next;
        free(to_be_removed);
    }
}

int get(struct Node* head_ref, int index)
{
    for ( ; index > 0 ; index--)
        head_ref = head_ref->next;

    return head_ref->data;
}

struct Node* reversed(struct Node* head_ref)
{
    struct Node* start = NULL;
    while (head_ref != NULL)
    {
        push(&start, head_ref->data);
        head_ref = head_ref->next;
    }

    return start;
}

void reverse(struct Node** head_ref)
{
    struct Node* start = reversed(*head_ref);
    struct Node* node = *head_ref;
    struct Node* next_node = node->next;
    while (node->next != NULL)
    {
        free(node);
        node = next_node;
        next_node = node->next;
    }
    free(node->next);
    free(node);
    *head_ref = start;
}

int getLength(struct Node* node)
{
    int count = 0;

    while (node != NULL)
    {
        count++;
        node = node->next;
    }

    return count;
}

void printList(struct Node* node)
{
    while (node != NULL)
    {
        printf("%d\n", node->data);
        node = node->next;
    }
}

int main()
{
    struct Node* start = NULL;

    int arr[] = {10, 20, 30, 40, 50}, i;
    for (i = 0 ; i <= 4 ; i++)
        push(&start, arr[i]);
    printf("Created integer linked list is \n");
    printList(start);
    printf("\n");
    struct Node* rev_start = reversed(start);
    printList(rev_start);
    printf("\n");
    reverse(&rev_start);
    printList(rev_start);

    return 0;
}

