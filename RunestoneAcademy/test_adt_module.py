from unittest import TestCase # import standard library
                              # third party libraries
from adt import Stack         # our own stuff


class ADTtestSuite(TestCase):
    
    def setUp(self):
        self.S = Stack()
    
    def test_create_stack(self):
        self.assertEqual(str(type(self.S)), "<class 'adt.Stack'>")
