#include <stdio.h>
#include <stdlib.h>

#define HEAPSIZE 100



void heapify(int* array, int pos)
{
    int left = (2 * pos) + 1;
    int right = (2 * pos) + 2;
    int last = array[HEAPSIZE];

    int largest_pos = pos;
    if (left <= last && array[left] > array[pos])
        largest_pos = left;
    if (right <= last && array[right] > array[largest_pos])
        largest_pos = right;

    if (largest_pos != pos)
    {
        int placeholder = array[pos];
        array[pos] = array[largest_pos];
        array[largest_pos] = placeholder;
        heapify(array, largest_pos);
    }
}

int* build_heap(int* array, int len)
{
    int* heap = malloc(sizeof(int) * (HEAPSIZE + 1));
    for (int i = 0 ; i < len ; i++)
        heap[i] = array[i];
    heap[HEAPSIZE] = len - 1;

    int last_parent_pos = (heap[HEAPSIZE] - 1) / 2;
    for (int pos = last_parent_pos ; pos >= 0 ; pos--)
        heapify(heap, pos);

    return heap;
}

void add_to_heap(int* heap, int item)
{
    heap[HEAPSIZE]++;
    heap[heap[HEAPSIZE]] = item;

    int parent_pos = heap[HEAPSIZE] - 1;
    while (parent_pos > 0)
    {
        parent_pos = parent_pos / 2;
        heapify(heap, parent_pos);
    }
}

int remove_max(int* heap)
{
    int old_max = heap[0];
    heap[0] = heap[heap[HEAPSIZE]];
    heap[HEAPSIZE]--;
    heapify(heap, 0);
    return old_max;
}

int main()
{
    int wannabeheap[] = {11, 2, 9, 37, 7, 42, 12};
    int *real_heap = build_heap(wannabeheap, 7);
    for (int i = 0 ; i <= real_heap[HEAPSIZE] ; i++)
        printf("%d, ", real_heap[i]);
    putchar('\n');
    add_to_heap(real_heap, 39);
    add_to_heap(real_heap, 99);
    for (int i = 0 ; i <= real_heap[HEAPSIZE] ; i++)
        printf("%d, ", real_heap[i]);
    putchar('\n');
    printf("%d\n", remove_max(real_heap));
    for (int i = 0 ; i <= real_heap[HEAPSIZE] ; i++)
        printf("%d, ", real_heap[i]);
    putchar('\n');
}

